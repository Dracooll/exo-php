	<?php

	class DB_Functions {

	private $db;

	//put your code here
	// constructor
	function __construct() {
		include_once './db_connect.php';
		
		// connecting to database
		$this->db = new DB_Connect();
		$this->db->connect();
	}

	// destructor
	function __destruct() {
		 
	}

	public function insertBanner($Image_Name,$Video_Name) {
		

		// insert user into database
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE) or die("connection failed");
		
		$result = mysqli_query($con,"INSERT INTO tbl_banner(Image,Video_URL) 
		VALUES('$Image_Name','$Video_Name')");
		// check for successful store
		if ($result) {
			// get user details
			$id = mysqli_insert_id($con); // last inserted id
			$result = mysqli_query($con,"SELECT * FROM tbl_banner WHERE Id = $id") or die(mysqli_error());
			// return user details
			if (mysqli_num_rows($result) > 0) {
				
				echo json_encode(array("success" => true, "message"=>"Successfully submited your record"));
				//return mysqli_fetch_array($result);
			} else {
			
				return false;
			}
		} else {
			return false;
		}

	}
	
	public function insertTrending($Video_Name,$Title,$Description) {
		

		// insert user into database
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE) or die("connection failed");
		
			$result = mysqli_query($con,"INSERT INTO tbl_trending(Vid,Title,Description,Video_URL) 
		VALUES('','$Title','$Description','$Video_Name')");
		// check for successful store
		if ($result) {
			// get user details
			$id = mysqli_insert_id($con); // last inserted id
			$result = mysqli_query($con,"SELECT * FROM tbl_trending WHERE Id = $id") or die(mysqli_error());
			// return user details
			if (mysqli_num_rows($result) > 0) {
				
				echo json_encode(array("success" => true, "message"=>"Successfully submited your record"));
				//return mysqli_fetch_array($result);
			} else {
			
				return false;
			}
		} else {
			return false;
		}

	}
	
		public function insertVideos($Category,$Video_Name,$Title,$Description) {
		

		// insert user into database
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE) or die("connection failed");
		
			$result = mysqli_query($con,"INSERT INTO tbl_videos(Category_Id,Image,Video,Title,Description) 
		VALUES('$Category','','$Video_Name','$Title','$Description')");
		// check for successful store
		if ($result) {
			// get user details
			$id = mysqli_insert_id($con); // last inserted id
			$result = mysqli_query($con,"SELECT * FROM tbl_videos WHERE Id = $id") or die(mysqli_error());
			// return user details
			if (mysqli_num_rows($result) > 0) {
				
				echo json_encode(array("success" => true, "message"=>"Successfully submited your record ".$id));
				mysqli_query($con,"update tbl_category set Timestamp=now() where Category_Id='$Category'");
				//return mysqli_fetch_array($result);
			} else {
			
				return false;
			}
		} else {
			return false;
		}

	}
	
	function updateImage($id,$imageName){
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE) or die("connection failed");
		$result = mysqli_query($con,"update tbl_videos set Image='$imageName' where Id='$id'");
		if($result){
       // echo json_encode(array("success" => true, "message"=>"Successfully Updated",));
		 $result = mysqli_query($con,"SELECT * FROM tbl_videos WHERE id = $id") or die(mysqli_error());
            // return user details
            if (mysqli_num_rows($result) > 0) {
				//while($row = $result->fetch_assoc()) {
				//$array[]=array("id"=> $row['Id'],"name" => $row['Name'],"email"=>$row['Email'],"mobile"=>$row['Mobile'],"pic"=>$row['Profile_Pic']);
				//}
				echo json_encode(array("success" => true, "message"=>"Successfully updated your image"));
                //return mysqli_fetch_array($result);
            }
      } else {
       echo json_encode(array("success" => false, "message"=>"Update Failed!!"));
       }
        return $result;
	}
	
	function updateViews($id,$count){
		$count++;
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE) or die("connection failed");
		$result = mysqli_query($con,"update tbl_videos set Views='$count' where Id='$id'");
		if($result){
       // echo json_encode(array("success" => true, "message"=>"Successfully Updated",));
		 $result = mysqli_query($con,"SELECT * FROM tbl_videos WHERE id = $id") or die(mysqli_error());
            // return user details
            if (mysqli_num_rows($result) > 0) {
				//while($row = $result->fetch_assoc()) {
				//$array[]=array("id"=> $row['Id'],"name" => $row['Name'],"email"=>$row['Email'],"mobile"=>$row['Mobile'],"pic"=>$row['Profile_Pic']);
				//}
				echo json_encode(array("success" => true, "message"=>"Successfully updated your views"));
                //return mysqli_fetch_array($result);
            }
      } else {
       echo json_encode(array("success" => false, "message"=>"Update Failed!!"));
       }
        return $result;
	}
	
	
	public function getBanner(){
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE) or die("connection failed");
		$result = mysqli_query($con,"select * from tbl_banner ORDER by Id DESC");
		return $result;
	}
	
	public function getTrending(){
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE) or die("connection failed");
		$result = mysqli_query($con,"select * from tbl_trending");
		return $result;
	}
	
	public function getVersion(){
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE) or die("connection failed");
		$result = mysqli_query($con,"select * from tbl_version");
		return $result;
	}
	
	public function getCategory(){
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE) or die("connection failed");
		//$result = mysqli_query($con,"select DISTINCT Category_Id from tbl_videos");
		$result = mysqli_query($con,"SELECT DISTINCT tbl_videos.Category_Id, tbl_category.Category_Name from tbl_videos 
		INNER JOIN tbl_category ON tbl_videos.Category_Id = tbl_category.Category_Id ORDER by tbl_category.Timestamp DESC");
		return $result;
	}
	
	public function getVideos($categoryId){
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE) or die("connection failed");
		//$result = mysqli_query($con,"select * from tbl_advertiser where Category =?");
		if ($stmt = $con->prepare("select Id,Category_Id,Image,Video,Title,Description,Views from tbl_videos where 
		Category_Id =? ORDER by Id DESC LIMIT 2")) {

		$stmt->bind_param('s', $categoryId);

		$stmt->execute();
		$stmt->store_result();
	  
	  

		}
		return $stmt;
	}
	
		public function getVideos2($categoryId){
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE) or die("connection failed");
		//$result = mysqli_query($con,"select * from tbl_advertiser where Category =?");
		if ($stmt = $con->prepare("select Id,Category_Id,Image,Video,Title,Description,Views from tbl_videos where Category_Id =?")) {
		$stmt->bind_param('s', $categoryId);
		$stmt->execute();
		$stmt->store_result();
		}
		return $stmt;
		}
		
		public function searchVideo($name){
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE) or die("connection failed");
		//$result = mysqli_query($con,"select * from tbl_advertiser where Category =?");
		$likeVar = "%" . $name . "%";
		if ($stmt = $con->prepare("select Id,Category_Id,Image,Video,Title,Description,Views from tbl_videos where Title LIKE ? OR Description LIKE ?")) {
		$stmt->bind_param('ss', $likeVar,$likeVar);
		$stmt->execute();
		$stmt->store_result();
		}
		return $stmt;
		}
		
		public function getVideoById($vid){
		require_once 'config.php';
		$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD,DB_DATABASE) or die("connection failed");
		//$result = mysqli_query($con,"select * from tbl_advertiser where Category =?");
		if ($stmt = $con->prepare("select Views from tbl_videos where Id =?")) {
		$stmt->bind_param('s', $vid);
		$stmt->execute();
		$stmt->store_result();
		}
		return $stmt;
		}
}
	?>