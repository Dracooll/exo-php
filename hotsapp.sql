-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2021 at 07:05 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotsapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_banner`
--

CREATE TABLE `tbl_banner` (
  `Id` int(11) NOT NULL,
  `Image` varchar(200) NOT NULL,
  `Video_URL` varchar(200) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_banner`
--

INSERT INTO `tbl_banner` (`Id`, `Image`, `Video_URL`, `Timestamp`) VALUES
(1, 'XYZ', 'XYZ', '2021-02-12 12:11:22'),
(2, 'XYZ', 'XYZ', '2021-02-12 12:14:48'),
(3, 'XYZ', 'XYZ', '2021-02-12 12:20:45'),
(4, 'download2.jpg', 'file_example_MP4_480_1_5MG.mp4', '2021-02-12 12:22:49'),
(5, 'download2.jpg', 'file_example_MP4_480_1_5MG.mp4', '2021-02-12 12:23:48'),
(6, 'download2.jpg', 'file_example_MP4_480_1_5MG.mp4', '2021-02-12 12:40:40');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `Id` int(11) NOT NULL,
  `Category_Id` varchar(200) NOT NULL,
  `Category_Name` varchar(200) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`Id`, `Category_Id`, `Category_Name`, `Timestamp`) VALUES
(1, '1', 'Movies', '2021-02-23 12:45:29'),
(2, '2', 'Songs', '2021-02-23 12:45:29'),
(3, '3', 'Trending', '2021-02-23 12:45:29'),
(4, '4', 'Comedy', '2021-02-23 12:45:29'),
(5, '5', 'Latest Movies', '2021-02-23 12:45:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_trending`
--

CREATE TABLE `tbl_trending` (
  `Id` int(11) NOT NULL,
  `Vid` varchar(200) NOT NULL,
  `Image` varchar(200) NOT NULL,
  `Title` varchar(200) NOT NULL,
  `Description` varchar(200) NOT NULL,
  `Video_URL` varchar(200) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_trending`
--

INSERT INTO `tbl_trending` (`Id`, `Vid`, `Image`, `Title`, `Description`, `Video_URL`, `Timestamp`) VALUES
(1, '', '', 'dsd', 'dfd', '', '2021-02-22 13:52:43'),
(2, '', '', 'dsd', 'dfd', '', '2021-02-22 13:52:43'),
(3, '', '', 'dsd', 'dfd', '', '2021-02-22 13:53:14'),
(4, '', '', 'dsd', 'dfd', '', '2021-02-22 13:53:14'),
(5, '', '', 'dsd', 'dfd', '', '2021-02-22 14:15:12'),
(6, '', '', 'dsd', 'dfd', '', '2021-02-22 14:27:49'),
(7, '', '', 'dsd', 'dfd', '', '2021-02-22 14:29:03'),
(22, '', '', 'dsd', 'dfd', '', '2021-02-22 14:48:54'),
(23, '', '', 'John', 'dfd', '', '2021-02-22 14:50:53'),
(24, '', '', 'John', 'Doe', 'file_example_MP4_480_1_5MG.mp4', '2021-02-22 14:54:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_videos`
--

CREATE TABLE `tbl_videos` (
  `Id` int(11) NOT NULL,
  `Category_Id` varchar(200) NOT NULL,
  `Image` varchar(200) NOT NULL,
  `Video` varchar(200) NOT NULL,
  `Title` varchar(200) NOT NULL,
  `Description` longtext NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_videos`
--

INSERT INTO `tbl_videos` (`Id`, `Category_Id`, `Image`, `Video`, `Title`, `Description`, `Timestamp`) VALUES
(1, '1', 'chola.webp', 'file_example_MP4_480_1_5MG.mp4', 'Sample', 'Sample', '2021-02-23 14:17:43'),
(2, '1', 'chola.webp', 'file_example_MP4_480_1_5MG.mp4', 'Sample2', 'Sample2', '2021-02-23 14:45:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_banner`
--
ALTER TABLE `tbl_banner`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbl_trending`
--
ALTER TABLE `tbl_trending`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tbl_videos`
--
ALTER TABLE `tbl_videos`
  ADD PRIMARY KEY (`Id`);
ALTER TABLE `tbl_videos` ADD FULLTEXT KEY `Description` (`Description`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_banner`
--
ALTER TABLE `tbl_banner`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_trending`
--
ALTER TABLE `tbl_trending`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tbl_videos`
--
ALTER TABLE `tbl_videos`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
